﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {
		
		String[] tickets= {"Einzelfahrschein Berlin AB",
							"Einzelfahrschein Berlin BC",
							"Einzelfahrschein Berlin ABC",
							"Kurzstrecke","Tageskarte Berlin AB",
							"Tageskarte Berlin BC","Tageskarte Berlin ABC",
							"Klengruppen-Tageskarte Berlin AB",
							"Kleingruppen-Tageskarte Berlin BC",
							"Kleingruppen-Tageskarte Berlin ABC"};
		
		double[] ticketpreise= {2.90,
								3.30,
								3.60,
								1.90,
								8.60,
								9.00,
								9.60,
								23.50,
								24.30, 
								24.90};
	
		
		String neuesTicket;
		char antwort='a';
		
		
		System.out.print("Hier sehen Sie eine Auswahl an verfügbaren Fahrkarten in Ihrer Stadt:\n\n");
		
		do {
			
			Scanner tastatur = new Scanner(System.in);

			double zuZahlenderBetrag=0;
			double eingezahlterGesamtbetrag=0;

			zuZahlenderBetrag += fahrkartenbestellungErfassen(tickets, ticketpreise);
				
			do {
 
				  System.out.println("Möchten Sie noch ein anderes Ticket kaufen?");
				  
				  neuesTicket = tastatur.next();
				  
				  antwort = neuesTicket.charAt(0);
				  
				  if (antwort == 'j' || antwort == 'J') {
					  
					  zuZahlenderBetrag += fahrkartenbestellungErfassen(tickets, ticketpreise);
				  }
				  
			}
				  while (wiederholung(antwort));			  
			
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			
			fahrkartenAusgabe();

			System.out.println("\n\n");		
			
			
			zuZahlenderBetrag = eingezahlterGesamtbetrag;
			
			rueckgeldAusgeben(zuZahlenderBetrag);			
			
			System.out.println("\n\nMöchten Sie weitere Fahrkarten erwerben?\nJa/Nein");

			neuesTicket = tastatur.next();
			
			antwort = neuesTicket.charAt(0);
			
			tastatur.close();

		}	while (wiederholung(antwort));
		
	}
	
	
	// Methode um zu prüfen, ob die Antwort die der Kunde gibt Ja oder Nein ist
	public static boolean wiederholung(char neuesTicket) {

		if (neuesTicket == 'j' || neuesTicket == 'J') {

			return true;

		}

		return false;

	}


	// Methodenkopf, Rückgabe-Datentyp, (Parameter) // Methode gibt den Ticketpreis an Hand des Eintrags im Array aus und prüft, ob die Eingabe gültig ist
	public static double fahrkartenbestellungErfassen(String[] tickets, double[] ticketpreise) {

		int eingabeTicket = 0;

		double zuZahlenderBetrag = 0;

		// Wenn Eingabe eines Users benötigt wird, dann wird Scanner benötigt

		Scanner tastatur = new Scanner(System.in);

		// System.out.print("Hier sehen Sie eine Auswahl an verfügbaren Fahrkarten in Ihrer Stadt:\n\n");


		for (int i = 1; i <= tickets.length; i = i + 1) {
			System.out.printf("%-34s: [%5.2f %-5s(%d)\n", tickets[i - 1], ticketpreise[i - 1], "EUR]", i);
		}

		System.out.println("\n\nWelche Fahrkarte möchten Sie erwerben?\nBitte geben Sie ihre Auswahl ein:_____");

		// definiert die Variable "eingabeTicket" nur dann, wenn die Nutzereingabe über
		// 0 und unter 10 ist, ansonsten wird er aufgefordert eine gültige Eingabe zu
		// machen

		do {

			eingabeTicket = tastatur.nextInt() - 1;

			if (eingabeTicket <= 10 || eingabeTicket > 0) {
				zuZahlenderBetrag = ticketpreise[eingabeTicket];
			}

			else
				System.out.println("Bitte geben Sie eine gültige Ticketnummer ein!");

		}

		while (eingabeTicket >= 10 || eingabeTicket < 0);

		System.out.print("Wie viele Tickets möchten Sie erwerben?: ");

		// nextByte damit die nächste Eingabe kein String ist, sondern ein Byte

		byte anzahlDerTickets = tastatur.nextByte();

		while (anzahlDerTickets > 10 || anzahlDerTickets < 0 || zuZahlenderBetrag < 0)

		{
			if (anzahlDerTickets > 10 || anzahlDerTickets < 0) {
				System.out.println(
						"ACHTUNG: Sie haben eine ungültige Ticketanzahl angegeben. Sie können nur zwischen 1 und 10 Tickets kaufen.\nBitte geben Sie eine gültige Anzahl ein:\n");
				anzahlDerTickets = tastatur.nextByte();
			}
			
			tastatur.close();

		}

		return zuZahlenderBetrag * anzahlDerTickets;
		

	}

	
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;

		eingezahlterGesamtbetrag = 0.00;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("Noch zu zahlen: "
					+ String.format("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag)) + " Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
			
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag + 0.00000001;

	}

	public static void muenzeAusgeben(int betrag, String einheit) {

		System.out.printf("\n   * * *\n"
						+ " *        *\n"
						+ "*%6d    *\n"
						+ "*%7s   *\n"
						+ " *        *\n"
						+ "    * * *", betrag, einheit);
		
		

	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag >= 0.00) {
			System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", rückgabebetrag) + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2;
			}

			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1;
			}

			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}

			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}

			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}

			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}

		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

	public static void warte(int z) {
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void fahrkartenAusgabe() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}

	}
}