import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		String meinArtikel;
		int meineAnzahl;
		double meinPreis;
		double mwst;
		double gesamtNetto;
		double gesamtBrutto;

		meinArtikel = liesString("was moechten Sie bestellen? ");
		meineAnzahl = liesInt("Geben Sie die Anzahl ein: ");
		meinPreis = liesDouble("Geben Sie den Nettopreis ein: ");
		mwst = liesDouble("Geben Sie denn Mehrwertsteuersatz in Prozent ein: ");
		gesamtNetto = berechneGesamtnettopreis(meineAnzahl,meinPreis);
		gesamtBrutto = berechneGesamtbruttopreis(gesamtNetto, mwst);
		liesAusgabe(meinArtikel,meineAnzahl,gesamtNetto,gesamtBrutto,mwst);
		
		

	}
	
	public static void liesAusgabe(String meinArtikel, int meineAnzahl, double gesamtNetto, double gesamtBrutto, double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", meinArtikel, meineAnzahl, gesamtNetto);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", meinArtikel, meineAnzahl, gesamtBrutto, mwst, "%");
		
	}
	
	
	
	public static String liesString(String text) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		
		return artikel;
	}
	
	public static int liesInt(String text) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		
		return anzahl;
	}

	public static double liesDouble(String text) {
	
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();
	
		return preis;
	
	}
	
	public static double berechneGesamtnettopreis(int meineAnzahl, double meinPreis) {
		
		return meineAnzahl * meinPreis;
				
	}
	
	public static double berechneGesamtbruttopreis(double gesamtNetto, double mwst) {
		
		return gesamtNetto * ( 1 + mwst / 100);
	}

	
}
