
public class Artikel {

	private int idNummer;
	private String bezeichnung;
	private double verkaufspreis;
	
	public Artikel() {
	
	}
	
	public Artikel(int id, String bezeichnung, double verkaufspreis) {
		this.idNummer = id;
		this.bezeichnung = bezeichnung;
		this.verkaufspreis = verkaufspreis;
	}
	
	public void setIdNummer(int id) {
		this.idNummer = id;
	}
	
	public int getIdNummer() {
		return this.idNummer;
	}
	
	
}


