package Raumschiffe;

import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungProzent;
	private int schildeProzent;
	private int huelleProzent;
	private int lebenserhaltungProzent;
	private int androidAnzahl;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();

	/**
	 * Vollparametisierter Konstruktur f�r die Klasse Raumschiff
	 * 
	 * @param schiffsname              der Name des Raumschiffs
	 * @param photonentorpedoAnzahl    die Anzahl der Photonentorpedos des
	 *                                 Raumschiffs
	 * @param energieversorgungProzent die Energieversorgung des Schiffes in %
	 * @param schildeProzent           die Schilde des Raumschiffs in %
	 * @param huelleProzent            die H�lle des Raumschiffs in %
	 * @param lebenserhaltungProzent   die Lebenserhaltungssysteme des Raumschiffs
	 *                                 in %
	 * @param androidAnzahl            die Anzahl der Droiden des Raumschiffs
	 */

	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungProzent, int schildeProzent,
			int huelleProzent, int lebenserhaltungProzent, int androidAnzahl) {

		this.schiffsname = schiffsname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungProzent = energieversorgungProzent;
		this.schildeProzent = schildeProzent;
		this.huelleProzent = huelleProzent;
		this.lebenserhaltungProzent = lebenserhaltungProzent;
		this.androidAnzahl = androidAnzahl;
	}

	/**
	 * Die Methode gibt das Ladungsverzeichnis in der Konsole aus, also alle
	 * Bezeichnungen und alle Mengen der geladenen Inhalte
	 */

	public void ladungsverzeichnisAusgeben() {
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println(ladung.getBezeichnung() + ": " + ladung.getMenge());

		}

	}

	/**
	 * die Methode gibt einen String auf der Konsole aus, dass das Raumschiff auf
	 * welches gezielt wurde getroffen wurde
	 * 
	 * @param r das Raumschiff das abgeschossen wird
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + " wurde getroffen!");
	}

	/**
	 * die Methode gibt eine message aus die vorher dem broadcastKommunikator
	 * hinzugef�gt wurde
	 * 
	 * @param message die message des broadcastKommunikators
	 */

	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
		System.out.println(message);
	}

	/**
	 * Wenn die Anzahl der Photonentorpedos die dem Schiff zur Verf�gung steht
	 * kleiner gleich 0 ist, so wird eine Benachrichtigung dar�ber ausgegeben.
	 * Ansonsten wird auf das vorgegebene Raumschiff geschossen und eine Nachricht
	 * ausgegeben.
	 * 
	 * @param r der Name des Raumschiffs
	 */
	public void torpedoSchiessen(Raumschiff r) {
		if (photonentorpedoAnzahl <= 0) {
			System.out.println("Keine Photonentorpedos mehr vorhanden!");
			nachrichtAnAlle("-=*Click*=-");
		} else {
			photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen!");
			treffer(r);
		}

	}

	/**
	 * Die Methode verschie�t die Phaserkanone, wenn die Energieversorgung des
	 * Schiffes gr��er als 50 ist, ansonsten gibt sie ein -=*Click*=- in der Konsole
	 * aus
	 * 
	 * @param r das Raumschiff auf welches geschossen wird
	 */
	public void kanoneSchiessen(Raumschiff r) {
		if (energieversorgungProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			energieversorgungProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen!");
			treffer(r);
		}
	}

	/**
	 * F�gt eine Ladung der ArrayList Ladung hinzu
	 * 
	 * @param ladung f�gt eine Ladung (Menge+Bezeichnung) einem Ladungsverzeichnis hinzu
	 */
	public void addLadung(Ladung ladung) {
		ladungsverzeichnis.add(ladung);
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungProzent() {
		return energieversorgungProzent;
	}

	public void setEnergieversorgungProzent(int energieversorgungProzent) {
		this.energieversorgungProzent = energieversorgungProzent;
	}

	public int getSchildeProzent() {
		return schildeProzent;
	}

	public void setSchildeProzent(int schildeProzent) {
		this.schildeProzent = schildeProzent;
	}

	public int getHuelleProzent() {
		return huelleProzent;
	}

	public void setHuelleProzent(int huelleProzent) {
		this.huelleProzent = huelleProzent;
	}

	public int getLebenserhaltungProzent() {
		return lebenserhaltungProzent;
	}

	public void setLebenserhaltungProzent(int lebenserhaltungProzent) {
		this.lebenserhaltungProzent = lebenserhaltungProzent;
	}

	public int getAndroidAnzahl() {
		return androidAnzahl;
	}

	public void setAndroidAnzahl(int androidAnzahl) {
		this.androidAnzahl = androidAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

}
