package Raumschiffe;

public class Ladung {

	private String bezeichnung;
	private int menge;

	/** Vollkonstruktur f�r die Klasse Ladung
	 * @param bezeichnung die Bezeichnung beziehungsweise den Namen der Ladung
	 * @param menge die Menge beziehungsweise die Anzahl der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}
		
	public String getLadungsinhalt() {
				
		String ladungsinhalt = menge + bezeichnung;
		
		return ladungsinhalt;
		
	}

}
