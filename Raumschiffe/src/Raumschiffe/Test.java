package Raumschiffe;

/**
 * @author Mirco
 * @version 1.0 vom 16.04.2021
 */

import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {

		Raumschiff klingonen = new Raumschiff("IKS Gegh'ta", 1, 100, 100, 100, 100, 2);
		Ladung schneck = new Ladung("Ferengi Schneckensaft", 200);
		Ladung bathleth = new Ladung("Bat'leth Klingonen Schwert", 200);
		klingonen.addLadung(schneck);
		klingonen.addLadung(bathleth);

		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		Ladung schrott = new Ladung("Borg-Schrott", 5);
		Ladung materie = new Ladung("Rote Materie", 2);
		romulaner.addLadung(materie);
		romulaner.addLadung(schrott);

		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 50, 100, 5);
		Ladung sonde = new Ladung("Forschungssonde", 35);
		vulkanier.addLadung(sonde);

		klingonen.torpedoSchiessen(romulaner);
		klingonen.setPhotonentorpedoAnzahl(1);
		romulaner.kanoneSchiessen(klingonen);
		romulaner.setEnergieversorgungProzent(100);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");

		Raumschiff Schiffe[] = { klingonen };
		ArrayList<Ladung> ladungsverzeichnis;
		for (int i = 0; i <= 0; i++) {
			System.out.println("Raumschiff: " + Schiffe[i].getSchiffsname() + "\nPhotonentorpedos: "
					+ Schiffe[i].getPhotonentorpedoAnzahl() + "\nSchilde bei " + Schiffe[i].getSchildeProzent()
					+ "%\nH�lle bei " + Schiffe[i].getHuelleProzent() + "%\nLebenserhaltungssysteme bei "
					+ Schiffe[i].getLebenserhaltungProzent() + "%\nReperatur Androiden: "
					+ Schiffe[i].getAndroidAnzahl());

			System.out.println();

			ladungsverzeichnis = Schiffe[i].getLadungsverzeichnis();
			for (Ladung Ausgabe : ladungsverzeichnis) {
				;
				System.out
						.println("Ladungsinhalt: " + Ausgabe.getBezeichnung() + "Ladungsmenge: " + Ausgabe.getMenge());
			}

			System.out.println("\n");
		}
		
		klingonen.torpedoSchiessen(romulaner);
		klingonen.torpedoSchiessen(romulaner);
		
		Raumschiff alleSchiffe[] = { klingonen, romulaner, vulkanier };
		
		for (int i = 0; i <= 0; i++) {
			System.out.println("Raumschiff: " + alleSchiffe[i].getSchiffsname() + "\nPhotonentorpedos: "
					+ alleSchiffe[i].getPhotonentorpedoAnzahl() + "\nSchilde bei " + alleSchiffe[i].getSchildeProzent()
					+ "%\nH�lle bei " + alleSchiffe[i].getHuelleProzent() + "%\nLebenserhaltungssysteme bei "
					+ alleSchiffe[i].getLebenserhaltungProzent() + "%\nReperatur Androiden: "
					+ alleSchiffe[i].getAndroidAnzahl());

			System.out.println();

			ladungsverzeichnis = alleSchiffe[i].getLadungsverzeichnis();
			for (Ladung Ausgabe : ladungsverzeichnis) {
				;
				System.out
						.println("Ladungsinhalt: " + Ausgabe.getBezeichnung() + "Ladungsmenge: " + Ausgabe.getMenge());
			}

			System.out.println("\n");
		}
		

		/*
		 * 
		 * System.out.println("Name: '" + klingonen.getSchiffsname() + "'");
		 * System.out.println("Anzahl Photonentorpedos: " +
		 * klingonen.getPhotonentorpedoAnzahl());
		 * System.out.println("Energieversorgung in Prozent: " +
		 * klingonen.getEnergieversorgungProzent());
		 * System.out.println("Schilde in Prozent: " + klingonen.getSchildeProzent());
		 * System.out.println("H�lle in Prozent: " + klingonen.getHuelleProzent());
		 * System.out.println("Lebenserhaltung in Prozent: " +
		 * klingonen.getLebenserhaltungProzent());
		 * System.out.println("Anzahl Androiden an Board: " +
		 * klingonen.getAndroidAnzahl()); System.out.println();
		 * System.out.println("Ladungsinhalt: " + schneck.getBezeichnung() +
		 * ", Anzahl: " + schneck.getMenge()); System.out.println("Ladungsinhalt: " +
		 * bathleth.getBezeichnung() + ", Anzahl: " + bathleth.getMenge());
		 * 
		 * System.out.println(); System.out.println();
		 * 
		 * System.out.println("Name: '" + romulaner.getSchiffsname() + "'");
		 * System.out.println("Anzahl Photonentorpedos: " +
		 * romulaner.getPhotonentorpedoAnzahl());
		 * System.out.println("Energieversorgung in Prozent: " +
		 * romulaner.getEnergieversorgungProzent());
		 * System.out.println("Schilde in Prozent: " + romulaner.getSchildeProzent());
		 * System.out.println("H�lle in Prozent: " + romulaner.getHuelleProzent());
		 * System.out.println("Lebenserhaltung in Prozent: " +
		 * romulaner.getLebenserhaltungProzent());
		 * System.out.println("Anzahl Androiden an Board: " +
		 * romulaner.getAndroidAnzahl()); System.out.println();
		 * System.out.println("Ladungsinhalt: " + schrott.getBezeichnung() +
		 * ", Anzahl: " + schrott.getMenge()); System.out.println("Ladungsinhalt: " +
		 * materie.getBezeichnung() + ", Anzahl: " + materie.getMenge());
		 * 
		 * System.out.println(); System.out.println();
		 * 
		 * System.out.println("Name: '" + vulkanier.getSchiffsname() + "'");
		 * System.out.println("Anzahl Photonentorpedos: " +
		 * vulkanier.getPhotonentorpedoAnzahl());
		 * System.out.println("Energieversorgung in Prozent: " +
		 * vulkanier.getEnergieversorgungProzent());
		 * System.out.println("Schilde in Prozent: " + vulkanier.getSchildeProzent());
		 * System.out.println("H�lle in Prozent: " + vulkanier.getHuelleProzent());
		 * System.out.println("Lebenserhaltung in Prozent: " +
		 * vulkanier.getLebenserhaltungProzent());
		 * System.out.println("Anzahl Androiden an Board: " +
		 * vulkanier.getAndroidAnzahl()); System.out.println();
		 * System.out.println("Ladungsinhalt: " + sonde.getBezeichnung() + ", Anzahl: "
		 * + sonde.getMenge());
		 * 
		 * klingonen.kanoneSchiessen(vulkanier);
		 * 
		 */

		/*
		 * Raumschiff Schiffe[] = {klingonen}; ArrayList<Ladung> ladungsverzeichnis;
		 * for(int i = 0; i <= 0; i++){ System.out.println( "Raumschiff: " +
		 * Schiffe[i].getSchiffsname() + "\nPhotonentorpedos: " +
		 * Schiffe[i].getPhotonentorpedoAnzahl() + "\nSchilde bei " +
		 * Schiffe[i].getSchildeProzent() + "%\nH�lle bei " +
		 * Schiffe[i].getHuelleProzent() + "%\nLebenserhaltungssysteme bei " +
		 * Schiffe[i].getLebenserhaltungProzent() + "%\nReperatur Androiden: " +
		 * Schiffe[i].getAndroidAnzahl());
		 * 
		 * System.out.println();
		 * 
		 * ladungsverzeichnis = Schiffe[i].getLadungsverzeichnis(); for (Ladung Ausgabe
		 * : ladungsverzeichnis) { ;System.out.println("Ladungsinhalt: " +
		 * Ausgabe.getBezeichnung() + "Ladungsmenge: " + Ausgabe.getMenge()); }
		 * 
		 * System.out.println("\n");
		 * 
		 * }
		 * 
		 */
	}

}
