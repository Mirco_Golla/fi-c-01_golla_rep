package konsolenausgabe;

public class test {
		
	public static void main(String[] args) {
		
		// = Zuweisung
		// int zahl3, zahl4
		// Operatoren: 
		//	= Zuweisung
		//	. Aufrufen einer Methode
		//	new neue Instanz von einem Objekt einer Klasse
		// artithmetische Operatoren: +, -, *, /, %
		//							/ = dividieren, wenn allerdings 40 / 7 geteilt wird, dann kommt 5 raus, obwohl es 5,714... sind
		//							% = modulo dividieren, bei 10%3 kommt 1 raus, weil 3x3 = 9, aber 1 Rest
		//							% = modulo division ist "teilen mit Rest"
		// Vergleichsoperatoren: >, <, !=, ==, >=, <=
		// 						Darstellung UND bzw ODER bzw NICHT: && Und, || ODER, ! NICHT
		
		int zahl1 = 23; //Deklaration und Initialisierung
		int zahl2 = 75; 
		
		// if Schlüsselwort zur Auswahl
		
		if (zahl1 < zahl2) {
			System.out.println("Zahl 1 ist kleiner als Zahl 2");
		}
		else if (zahl1 > zahl2) {
			System.out.println("Zahl 1 ist größer als Zahl 2");
		}
		
		else {
			System.out.println("Zahl 1 ist gleich Zahl 2");
		}
	}
}