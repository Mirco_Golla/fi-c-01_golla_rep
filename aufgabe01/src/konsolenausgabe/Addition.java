package konsolenausgabe;

import java.util.Scanner;

class Addition {

    // static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
    	
    	double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
    	
    	programmHinweis();
    	zahl1 = eingabe("bitte geben Sie die erste Zahl ein");    	
    	zahl2 = eingabe("bitte geben Sie die zweite Zahl ein");
    	erg = addition(zahl1, zahl2);
    	ausgabe(zahl1, zahl2, erg);
    }
    public static void programmHinweis() {
    	
    	System.out.println("Hinweis: ");
    	System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    	
    }

    public static double eingabe(String text) {
    	
    	Scanner sc = new Scanner(System.in);
    	    	   	
    	System.out.println(text);
        double zahl = sc.nextDouble();
        
        return zahl;
    }

    public static double addition(double zahl1, double zahl2) {
    	
    	double erg = zahl1 + zahl2;
    	
    	return erg;
    }

    public static void ausgabe(double erg, double zahl1, double zahl2) {
    	
    	System.out.println("Ergebnis der Addition");
    	System.out.printf("%.2f + %.2f = %.2f", zahl1, zahl2, erg);
    }
}