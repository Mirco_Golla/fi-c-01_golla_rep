package konsolenausgabe;

import java.util.Scanner;

public class test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] tickets= {"Einzelfahrschein Berlin AB",
				"Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC",
				"Kurzstrecke","Tageskarte Berlin AB",
				"Tageskarte Berlin BC","Tageskarte Berlin ABC",
				"Klengruppen-Tageskarte Berlin AB",
				"Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC"};
		
		double[] ticketpreise= {2.9,
				3.30,
				3.60,
				1.90,
				8.60,
				9.00,
				9.60,
				23.50,
				24.30, 
				24.90};

		System.out.print("Hier sehen Sie eine Auswahl an verf�gbaren Fahrkarten in Ihrer Stadt:\n\n");
		
		for(int i=1 ; i <= tickets.length; i=i+1)
		{		
		System.out.printf("%-34s: [%5.2f %-5s(%d)\n", tickets[i-1], ticketpreise[i-1],"EUR]", i);	
		}
		
		System.out.println("\n\nWelche Fahrkarte m�chten Sie erwerben?\nBitte geben Sie ihre Auswahl ein:_____");

	}
}
