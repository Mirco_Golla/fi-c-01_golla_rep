package konsolenausgabe;

public class Aufgabe01 {

	public static void main(String[] args) {
		System.out.println("   **");
		System.out.print("*");
		System.out.println("      *");
		System.out.print("*");
		System.out.println("      *");
		System.out.println("   **");
		
		String s = "*";
		String S = "**";
		System.out.printf( "%5s\n", S );
		System.out.printf( "%8s\n", s );

	}

}
